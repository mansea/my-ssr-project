import { EggPlugin } from 'egg'

const proxyagent = {
  enable: true,
  package: 'egg-development-proxyagent'
}

export default {
  proxyagent,
  static: true,
  security: false,
  // security: {
  //   csrf: {
  //     headerName: 'x-csrf-token'
  //   },
  //   xframe: {
  //     enable: false
  //   },
  // },
} as EggPlugin;