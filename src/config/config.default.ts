import { join } from 'path'
import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg'

export type DefaultConfig = PowerPartial<EggAppConfig>
export const axios = {
	baseURL: 'http://127.0.0.1:3000/',
  // `headers` are custom headers to be sent
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  },
  timeout: 1000, // default is `0` (no timeout)

  // `withCredentials` indicates whether or not cross-site Access-Control requests
  // should be made using credentials
  withCredentials: false, // default
};

export const configApi = {
  crm: 'http://192.168.1.33:8098',
  portal: 'http://192.168.1.33:8084',
  uc: 'http://192.168.1.33:8088',
}

export default (appInfo: EggAppInfo) => {
  const config = {} as DefaultConfig

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1611038425326_4049'
  // add your config here
  config.middleware = []
  config.static = {
    prefix: '/',
    dir: [join(appInfo.appDir, './build'), join(appInfo.appDir, './public'), join(appInfo.appDir, './build/client')]
  }
  return config
}
