import { Readable } from 'stream'
import { Controller, Get, Provide, Inject } from '@midwayjs/decorator'
import { Context } from 'egg'
import { render } from 'ssr-core-vue3'
import { IApiService, IApiHomeService } from '../../interface'

interface IEggContext extends Context {
  apiService: IApiService
  apiHomeservice: IApiHomeService
}

@Provide()
@Controller('/home')
export class HOME {
  @Inject()
  ctx: IEggContext

  @Inject('ApiService')
  apiService: IApiService

  @Inject('ApiCrmService')
  apiHomeservice: IApiHomeService

  @Get('/detail/:id')
  async handler (): Promise<void> {
    try {
      this.ctx.apiService = this.apiService
      this.ctx.apiDeatilservice = this.apiHomeservice
      const stream = await render<Readable>(this.ctx, {
        stream: true
      })
      this.ctx.body = stream
    } catch (error) {
      this.ctx.body = error
    }
  }
}
