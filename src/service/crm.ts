import { Provide, Inject, Logger, Config } from '@midwayjs/decorator'
import { Ddata } from '~/typings/data'
import mock from '../mock/detail'
import { HttpService } from '@midwayjs/axios';
import { ILogger } from '@midwayjs/logger';
import { Context } from 'egg'

@Provide('ApiCrmService')
export class ApiCrmService {
  @Inject()
  ctx: Context;
  @Inject()
  httpService: HttpService;
  @Logger()
  logger: ILogger;
  @Config('configApi')
  configApi;
  async index (id): Promise<Ddata> {
    return await Promise.resolve(mock.data[id])
  }

  async invoke() {
    const url = this.configApi['crm'] + '/tQuotedPrice/v1/getQuotationInfo';
    // const url = this.configApi['crm'] + '/tQuotedPrice/v1/getQuotationInfo?param=eyJpZCI6IjU2OTliNTQzNzVkMDQ4YjY5ZGFhMWE1ZjQzMGExMjhhIiwidmVyc2lvbiI6MX0=';
    const config = {
      params: {
        param: 'eyJpZCI6IjU2OTliNTQzNzVkMDQ4YjY5ZGFhMWE1ZjQzMGExMjhhIiwidmVyc2lvbiI6MX0='
      }
    }
    const result = await this.httpService.get(url, config);

    return result;
  }
}
