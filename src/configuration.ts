import { Configuration, Logger } from '@midwayjs/decorator';
import * as axios from '@midwayjs/axios';
import { join } from 'path';
import { IMidwayContainer } from '@midwayjs/core';
import { ILogger } from '@midwayjs/logger';

@Configuration({
  imports: [
    axios		// 导入 axios 组件
  ],
  importConfigs: [
    join(__dirname, 'config')
  ]
})
export class ContainerLifeCycle {
  @Logger()
  logger: ILogger;
  
  async onReady(container: IMidwayContainer) {
  	const httpService = await container.getAsync(axios.HttpService);
    httpService.interceptors.request.use(
      (config: any) => {
        const currentUser = {
          token: ''
        }
        config.headers['Accept-Language'] = "zh-CN";
        config.headers.Authorization = `Bearer ${currentUser.token}`
        config.headers.token = `Bearer ${currentUser.token}`
        config.headers['x-csrf-token'] = `Bearer ${currentUser.token}`
        config.responseType = 'json';
        // Do something before request is sent
        return config;
      },
      error => {
        // Do something with request error
        return Promise.reject(error);
      }
    );
    httpService.interceptors.response.use(
      (res: any) => {
        if (res.config.method === 'get' || res.config.method === 'GET') {
          this.logger.info('url:' + res.config.url + ' method:' + res.config.method + ' params:' + JSON.stringify(res.config.params));
        } else {
          this.logger.info('url:' + res.config.url + ' method:' + res.config.method + ' data:' + JSON.stringify(res.config.data));
        }
        if (res && res.status && Number(res.status) === 200) {
          return res.data;
        }

        return res;
      },
      (error: any) => {
        this.logger.error('url:' + error.response.config.url + ' method:' + error.response.config.method + ' data:' + error.response.config.data);
        this.logger.error(error.response.data);
        // Do something with request error
        return Promise.reject(error);
      }
    );
  }
}