export interface IApiHomeService {
  index: (id: string) => Promise<any>,
  invoke: () => Promise<any>
}
