import { Inject, Controller, Provide, Get, Post } from '@midwayjs/decorator'
import { Context } from 'egg'
import { IApiService, IApiHomeService } from '../../interface'

@Provide()
@Controller('/api')
export class HomeApi {
  @Inject()
  ctx: Context

  @Inject('ApiService')
  service: IApiService

  @Inject('ApiCrmService')
  homeService: IApiHomeService

  @Get('/detail/:id')
  async getDetailData () {
    const { ctx, homeService } = this
    const id = ctx.params.id
    const data = await homeService.index(id)
    // const data1 = await detailService.invoke()
    // console.log('data', data1);
    return data
  }
  @Post('/detail1/:id')
  async postDetailData (ctx: Context) { // ctx上下文  ctx.request.body获取接口post参数  ctx.params获取get参数  ctx.query获取get参数
    const { homeService } = this
    const id = ctx.params.id
    const data = await homeService.index(id)
    // const data1 = await detailService.invoke()
    // console.log('data', data1);
    return data
  }
  // @Get('/detail/:id')
  // async getDetailData () {
  //   const { ctx, detailService } = this
  //   const id = ctx.params.id
  //   const data = await detailService.index(id)
  //   return data
  // }
}
