
export default {
  async request(data: any) {
    console.log('request', data);
    let reqData = data.data || {};
    return (await (window.fetch(data.url, {
      method: data.method || 'GET',
      body: JSON.stringify(reqData),
      headers: data.headers
    }))).json()
  },
  download(url: String) {
    return this.request({ url, responseType: "arraybuffer" });
  },
  get(url: String, type: String, isShowMessage: Boolean) {
    return this.request({ url, responseType: type || 'json' });
  },
  post(url: String, data: any, responseType: String) {
    return this.request({ url, data, method: "POST", responseType: responseType || 'json' });
  },
  put(url: String, data: any) {
    return this.request({ url, data, method: "PUT" });
  },
  remove(url: String, data: any) {
    return this.request({ url, data, method: "DELETE" });
  }
};