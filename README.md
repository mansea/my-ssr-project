# 官方文档

midway-vue3-ssr官方文档请查看 [http://doc.ssr-fc.com/](http://doc.ssr-fc.com/)
midwayjs官方文档请查看 [https://www.yuque.com/midwayjs/midway_v2]
midwayjs官方文档请查看 [https://www.yuque.com/midwayjs]
eggjs官方文档请查看 [https://eggjs.org/zh-cn/]
vue3官方文档请查看 [https://v3.cn.vuejs.org/]

## node使用版本
node >= 14.17.0
小于14.17.0 vite运行不生效，npm run start:vite

## getting start

```bash
$ npm start # 本地开发模式运行，单进程 支持 前端 HMR 前端静态资源走本地 webpack 服务
$ npm run prod # 模拟生产环境运行，多进程，前端资源走静态目录
$ npm run stop # 生产环境停止服务
```


## 启用W2进行接口代理实时查看
```bash
npm i whistle -g
```
Open new cmd
```bash
w2 run -p 9877
```
浏览器打开127.0.0.1:9877
同步74HOST，以及进行https 反代理为 http操作

## 项目结构
````script
├── pubilc 
├── src // 服务端相关
│   ├── config // 配置
│   │   ├── config.default.ts
│   │   ├── config.local.ts
│   │   └── plugin.ts
│   ├── api // 接口路由地址
│   │   ├── home
│   │   │   └── home.ts // 接口路由
│   │   │ 
│   │   └── index.ts
│   ├── controller // 控制器
│   │   ├── home
│   │   │   └── home.ts // 页面路由
│   │   │ 
│   │   └── index.ts
│   ├── interface // 类型定义
│   │   ├── home.ts
│   │   └── index.ts
│   ├── mock // mock数据
│   │   ├── detail.ts
│   │   └── index.ts
│   ├── app.ts // 
│   ├── configuration.ts // 
│   └── service // 接口服务
│       ├── crm.ts
│       └── index.ts
├── typings  
├── web // 页面相关
│   ├── @types // 全局类型定义
│   │   ├── global.d.ts
│   │   └── typings.d.ts
│   ├── components // 组件
│   │   ├── layout // 全局模板（inde.html）
│   │   │   ├── App.vue
│   │   │   ├── favicon.ico
│   │   │   ├── fetch.ts
│   │   │   └── index.vue
│   │   └── slider 组件
│   ├── pages // 页面
│   │   ├── home
│   │   │    ├── fetch.ts 
│   │   │    └── render.vue  // 页面
│   │   ├── index
│   │       ├── fetch.ts
│   │       └── render.vue
│   ├── store // 状态存储
│   │   ├── modules
│   │   └── index.ts
│   ├── common.less // 全局样式
│   └── tsconfig.json
├── .eslintrc.js  
├── .eslintignore  
├── .gitignore  
├── package.json  
├── plugin.js  
├── config.ts  
├── README.md  
├── vetur.config.js 
└── tsconfig.json
````